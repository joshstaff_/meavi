<?php

namespace App\Support\Models;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;

abstract class ModelManager
{
    use ValidatesRequests;
    use DispatchesJobs;

    /**
     * Find a Model by Model ID (primary key).
     *
     * @param int $model
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find($model)
    {
        if ($model instanceof Model) {
            return $model;
        }

        try {
            return $this->findOrFail($model);
        } catch (ModelNotFoundException $exception) {
            $this->throws('Invalid');
        }
    }

    /**
     * Create a new Model from defined Data.
     *
     * @param array|\Illuminate\Http\Request $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($data)
    {
        $command = $this->command('create', $this->input($data));

        if ($command->fails()) {
            $this->throws('Invalid', 'Data', $command->validator(), $data);
        }

        return $this->dispatch($command);
    }

    /**
     * Update a Model from defined update data
     * and a defined Model Id (primary key).
     *
     * @param mixed                          $id
     * @param array|\Illuminate\Http\Request $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($model, $data)
    {
        $command = $this->command('update', $this->find($model), $this->input($data));

        if ($command->fails()) {
            $this->throws('Invalid', 'Data', $command->validator(), $data);
        }

        return $this->dispatch($command);
    }

    /**
     * Delete a Model defined by Model Id (primary key).
     *
     * @param int $model
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function delete($model)
    {
        return $this->dispatch(
            $this->command('delete', $this->find($model))
        );
    }

    /**
     * Returns the Input Data from a Request.
     *
     * If the data provided is not in the form of a
     * Request then we just return it anyway.
     *
     * @param mixed $data
     *
     * @return mixed
     */
    protected function input($data)
    {
        if ($data instanceof Request) {
            return $data->all();
        }

        return $data;
    }

    /**
     * Performs a new Model Command.
     *
     * Calls the Model Command using the defined
     * action and the provided parameters.
     *
     * @param string $action
     * @param array  $parameters
     *
     * @throws \App\Support\Models\ModelCommandNotFound
     *
     * @return
     */
    protected function command($action)
    {
        $command = $this->commandFrom($action);

        if (!class_exists($command)) {
            throw new ModelCommandNotFound();
        }

        return app($command, array_slice(func_get_args(), 1));
    }

    /**
     * Return a Fully Qualified Command Class from an Action.
     *
     * @param string $action
     *
     * @return string
     */
    protected function commandFrom($action)
    {
        return substr(get_called_class(), 0, strrpos(get_called_class(), '\\')).'\\Commands\\'.ucfirst($action).$this->getModelName();
    }

    /**
     * @param string $before
     * @param string $after
     * @throws ModelException
     * @throws ValidationException
     */
    protected function throws($before = '', $after = '')
    {
        $parameters = array_slice(func_get_args(), 2);

        if (isset($parameters[0]) && isset($parameters[1]) && $parameters[0] instanceof Validator && $parameters[1] instanceof Request) {
            throw new ValidationException($parameters[0], $this->buildFailedValidationResponse(
                $parameters[1], $this->formatValidationErrors($parameters[0])
            ));
        }

        if (isset($parameters[0]) && $parameters[0] instanceof Validator) {
            throw new ValidationException($parameters[1]);
        }

        $exception = substr(get_called_class(), 0, strrpos(get_called_class(), '\\')).'\\'.$before.$this->getModelName().$after;

        if (class_exists($exception)) {
            throw new $exception(array_slice(func_get_args(), 2));
        }

        if (class_exists($exception = $before.'Model'.$after)) {
            throw new $exception(array_slice(func_get_args(), 2));
        }

        throw new ModelException();
    }

    /**
     * The Model currently being Managed.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return app($this->getModelFullClass());
    }

    /**
     * The Model currently being Managed.
     *
     * Returns the Basename of the Model Class,
     * for instance if this class was 'UserManager',
     * this method will return 'User'.
     *
     * @return string
     */
    protected function getModelName()
    {
        return str_replace('Manager', '', class_basename($this));
    }

    /**
     * The Model currently being Managed.
     *
     * Returns the Basename of the Model Class,
     * for instance if this class was 'UserManager',
     * this method will return 'User'.
     *
     * @return string
     */
    protected function getModelFullClass()
    {
        return str_replace('Manager', '', get_class($this));
    }

    /**
     * Handle dynamic method calls into the Model Manager.
     *
     * @param string $method
     * @param array  $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return call_user_func_array([$this->model(), $method], $parameters);
    }
}
