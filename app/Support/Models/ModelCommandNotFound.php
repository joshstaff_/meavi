<?php

namespace App\Support\Models;

use Exception;

class ModelCommandNotFound extends Exception
{
}
