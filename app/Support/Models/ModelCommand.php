<?php

namespace App\Support\Models;

use App\Jobs\Job;
use Illuminate\Foundation\Bus\DispatchesJobs;

abstract class ModelCommand extends Job
{
    use DispatchesJobs;

    /**
     * Validation Rules.
     *
     * @var array
     */
    protected $validation;

    /**
     * Data Provided.
     *
     * @var array
     */
    protected $data;

    /**
     * Validator Instance.
     *
     * @var \Illuminate\Validation\Validator
     */
    public $validator;

    /**
     * Validation Rules.
     *
     * @return
     */
    public function validation()
    {
        return is_array($this->validation) ? $this->validation : [];
    }

    /**
     * Provided Data.
     *
     * Allows manipulation of data pre-validation.
     *
     * @return array
     */
    public function data()
    {
        return $this->data;
    }

    /**
     * Current Request.
     *
     * Allows access to the current request instance.
     *
     * @return array
     */
    public function request()
    {
        return app('request');
    }

    /**
     * Get Attribute from Provided Data.
     *
     * @return mixed
     */
    public function get($attribute, $default = null)
    {
        $data = $this->data();

        if (isset($data[$attribute])) {
            return $data[$attribute];
        }

        return $default;
    }

    /**
     * Get a File from the Request.
     *
     * @return \Illuminate\Http\UploadedFile
     */
    public function file($attribute)
    {
        return $this->request()->file($attribute);
    }

    /**
     * Validate if the Command can be run.
     *
     * @return bool
     */
    public function validate()
    {
        $validator = $this->validator();

        if ($validator->fails()) {
            return false;
        }

        return true;
    }

    /**
     * Does the data provided to the Command Pass Validation.
     *
     * @return bool
     */
    public function passes()
    {
        if ($this->validate()) {
            return true;
        }

        return false;
    }

    /**
     * Does the data provided to the Command Fail Validation.
     *
     * @return bool
     */
    public function fails()
    {
        if ($this->passes()) {
            return false;
        }

        return true;
    }

    /**
     * Returns the Validator instance if it exists,
     * otherwise creates the validator instance
     * with the command data and rules.
     *
     * @return
     */
    public function validator()
    {
        if ($this->validator) {
            return $this->validator;
        }

        return $this->validator = validator($this->data(), $this->validation());
    }
}
