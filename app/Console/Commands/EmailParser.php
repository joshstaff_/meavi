<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use MimeMailParser\Parser as MailParser;
use MimeMailParser\Attachment;
use Illuminate\Support\Facades\Storage;
use Smalot\PdfParser\Parser;

class EmailParser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $fd = fopen("php://stdin", "r");
        $rawEmail = "";
        while (!feof($fd)) {
            $rawEmail .= fread($fd, 1024);
        }
        fclose($fd);

        $parser = new MailParser();
        $parser->setText($rawEmail);
        $attachments = $parser->getAttachments();

        $toEmail =  $Parser->getHeader('to');

        if(!User::obtainMeaviEmail($toEmail)) {
            //This user does not have a meavi email address
        }

        //User exists. Happy path achieved.

        //Fancy attachment Storage fir the attachments....... Similiar to BC
        //Legals document storage trait. For now, chuck in a simple file put contents
        //We also need to store the users attachment against them.
        foreach ($attachments as $attachment) {
            file_put_contents(public_path('hello/'.$attachment->getFilename()), $attachment->getContent());
            // $attachment = some attachment
            //$user->attach($attachmentInstance);
        }

        //$user->save();
      }

      //Any instance of a user can be passed to this - Unsure if I wanna do it this way...
      //Likely i'll make it occur on an event when the user is created..
    public function hello(User $user) {
          $user = User::whereMeaviEmail($user->buildMeaviEmail())->first();
          if($user) {
            //lets try make some suggestions.....
            return $user->first_name.'-'.$this->obtainRandomWord();
          }

          $user->meavi_email = $user->buildMeaviEmail();
          $user->save();


      }
      //give the user a random word suggestion....
      private function obtainRandomWord() {
        $randomWords = [
          'traveler', 'excitable', 'bigdog', 'nicememes'
        ];
        return $randomWords[rand(0, count($randomWords))];
      }
}
