<?php

namespace Meavi;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register bindings in the container.
     */
    public function register()
    {
        $this->app->singleton(
            'meavi',
            function ($app) {
                return $app->make(Helper::class, [$app]);
            }
        );
    }
}
