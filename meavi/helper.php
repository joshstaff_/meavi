<?php

namespace Meavi;


use BadMethodCallException;
use Illuminate\Foundation\Application;
use Meavi\Users\UserManager;

class Helper {

    protected $helpers = [
      'User' => UserManager::class,
    ];

    /**
     * Application Instance.
     *
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    /**
     * __construct.
     *
     * @param \Illuminate\Foundation\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Application Instance.
     *
     * Return the Application Container.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function app()
    {
        return $this->app;
    }

    /**
     * Provide access to the Helper Methods.
     *
     * @param string $method
     * @param array  $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (array_key_exists($method, $this->helpers)) {
            return $this->callHelperMethod($method, $parameters);
        }

        $className = get_class($this);

        throw new BadMethodCallException("Call to undefined method {$className}::{$method}()");
    }

    /**
     * Call a Helper Method.
     *
     * @param string $method
     * @param mixed  $parameters
     *
     * @return mixed
     */
    protected function callHelperMethod($method, $parameters)
    {
        return $this->app->make($this->helpers[$method], $parameters);
    }
}