<?php

namespace Meavi\Users\Commands;

use App\Support\Models\ModelCommand;
use App\Users\User;

class BuildMeaviEmailCommand extends ModelCommand {

    /**
     * BuildMeaviEmailCommand constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->data = $user;
    }

    /**
     * Execute the command.
     *
     * @return User
     */
    public function handle()
    {
        dd('here');
    }
}