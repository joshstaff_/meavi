<?php

namespace Meavi\Users\Commands;

use App\Support\Models\ModelCommand;
use App\Users\User;

class RegisterUserCommand extends ModelCommand {

    /**
     * Validation Rules.
     *
     * @var array
     */
    protected $validation = [
        'first_name' => 'required|max:50',
        'last_name' => 'required|max:50',
        'email' => 'required|email|max:150|unique:users',
        'password' => 'required|min:8',
    ];

    /**
     * Create a new command instance.
     *
     * @param array $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the command.
     *
     * @return User
     */
    public function handle()
    {
        $user = new User(
            [
                'first_name' => $this->get('first_name'),
                'last_name' => $this->get('last_name'),
                'email' => $this->get('email'),
                'password' => bcrypt($this->get('password')),
            ]
        );


        dd($user);

//        $user->save();

//        return $user;
    }
}