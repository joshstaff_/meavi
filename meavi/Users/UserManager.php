<?php

namespace Meavi\Users;

use App\Support\Models\ModelManager;
use Meavi\Users\Commands\RegisterUserCommand;

class UserManager extends ModelManager
{


    /**
     * @var \Meavi\Users\User
     */
    protected $user;


    /**
     * UserManager constructor.
     * @param User $user
     */
    public function __construct (User $user)
    {
        $this->user = $user;
    }

    /**
     * Register the User
     *
     * @param $data
     * @return mixed
     */
    public function register($data)
    {
        $command = new RegisterUserCommand($this->input($data));

        if ($command->fails()) {
            $this->throws('Invalid', 'Data', $command->validator(), $data);
        }

        return $this->dispatch($command);
    }


}
