<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


use Meavi;
Route::get('/', function(){
//   $j = factory(\App\User::class)->create();
//
//    dd($j);
});

Route::get('/auth/login', function(Request $request){

});

Route::get('/auth/signup', function(Request $request){

//    $userCredentials = [
//        $request->get('email'),
//        $request->get('first_name'),
//        $request->get('last_name'),
//    ];

    Meavi::users()->register($request);
});

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
